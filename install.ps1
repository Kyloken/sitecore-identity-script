$prefix="sc902"
$CMurl="https://$prefix.sc"
$IdentityServer="$prefix.IdentityServer"
$ClientSecret="1111enFma0RGAp1K4Y2MT0GUiv37QudtkaKNnvYyE3zXzsYIN8Vtsr13E8lLzJm1f5ynqDdLk8yT65LYAZ5HO6lgtNCy5r01hywO"
$SqlServer = "KYLOKEN\SQL2016"
$SqlAdminUser = "sa"
$SqlAdminPassword="12345"
$PSScriptRoot = "D:\resourcefilesIdentity"


$certParamsForIdentityServer = @{
Path = "$PSScriptRoot\createcert.json"
CertificateName = $IdentityServer
}

Install-SitecoreConfiguration @certParamsForIdentityServer -Verbose


$identityParams = @{
Path="$PSScriptRoot\IdentityServer.json"
Package="$PSScriptRoot\Sitecore.IdentityServer.2.0.0-r00157.scwdp.zip"
SqlDbPrefix=$prefix
SqlServer=$SqlServer
SqlCoreUser=$SqlAdminUser
SqlCorePassword=$SqlAdminPassword
SitecoreIdentityCert=$certParamsForIdentityServer.CertificateName
Sitename=$IdentityServer
PasswordRecoveryUrl=$CMurl
AllowedCorsOrigins=$CMurl
ClientSecret=$ClientSecret
LicenseFile="$PSScriptRoot\license.xml"
}

Install-SitecoreConfiguration @identityParams